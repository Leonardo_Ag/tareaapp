package com.oitic.tarea12;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class otroActivity extends AppCompatActivity {

    static final int REQUEST_CALL = 1;
    EditText phone;

    //En esta parte se declaran los botones, siemopre se tienen que declarar aqui
    Button btn_return;
    Button btn_call;
    Button btn_contact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otro);

        //Se instancia los botos que fueron creados en el xml utilizando el boton declarado
        // en la parte de arriba y el id que debe de tener el boton

        phone = findViewById(R.id.txtphone);
        btn_return = findViewById(R.id.btnreturn);
        btn_call = findViewById(R.id.btnCall);
        btn_contact = findViewById(R.id.btnContact);

        //En esta parte se declara el onclic, es decir, el evento que sucedera al dar clic en el boton

        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity(v); // en esta parte se llama al metodo que contiene las acciones a realizar
            }
        });

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNumber();
            }
        });

        btn_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContacts();
            }
        });



    }
    //Aqui se crea el metodo para regresar al activity principal, el metodo intent funciona para mandar o dar la orden para que sea ejecutada
    public void MainActivity(View view) {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }
    //En este metodo se da la peticion para realizar una llamada
    public void callNumber() {

        String number = phone.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(otroActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(otroActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(otroActivity.this, "Ingresa el numero", Toast.LENGTH_SHORT).show();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callNumber();
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Aqui se abre la aplicacion contactos desde la aplicaccion creada
    public void openContacts() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));
        startActivity(intent);
    }

}
