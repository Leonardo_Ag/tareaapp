package com.oitic.tarea12;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class toast {

    //Metodo para mostrar el mensaje toast
    public void mostrarToast(Context context){
        Toast.makeText(context,"Toast dentro de Clase",Toast.LENGTH_LONG).show();
    }
}
