package com.oitic.tarea12;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    //En esta parte se declaran los botones, siemopre se tienen que declarar aqui
    Button btn_toast;
    Button btn_Activity;
    toast toastClass = new toast();
    Button btn_bus;
    EditText editTextWeb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se instancia o finquean los botones que fueron creados en el xml
        // utilizando el boton declarado en la parte de arriba
        // y el id que debe de tener el boton
        btn_toast = findViewById(R.id.btnToast);
        btn_Activity = findViewById(R.id.btnActivity);
        editTextWeb = (EditText) findViewById(R.id.editTextWeb);
        btn_bus = findViewById(R.id.btnBus);

        //En esta parte se declara el onclic, es decir, el evento que sucedera al dar clic en el boton

        btn_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastClass.mostrarToast(MainActivity.this); //se llama a la clase creada para mostrar el mensaje toast
            }
        });

        //Evento para buscar la url escrita
        btn_bus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl();
            }
        });

        //Evento para enviar al nuevo activity
       btn_Activity.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               otroActivity(v);
           }
       });
    }

    //Metodo para enviar a siguiente activity
    public void otroActivity(View view) {

        Intent intent = new Intent(this, otroActivity.class);
        startActivity(intent);

    }

    //Metodo para buscar la url escrita en el text view
    public void openUrl() {
        String url = editTextWeb.getText().toString();
        if (url != null && !url.isEmpty()) {
            Intent intentWeb = new Intent();
            intentWeb.setAction(Intent.ACTION_VIEW);
            intentWeb.setData(Uri.parse("http://"+url));
            startActivity(intentWeb);
        } else {
            Toast.makeText(MainActivity.this, "Ingresa la URL", Toast.LENGTH_SHORT).show();
        }
    }
}
